# PSU CS Capstone Personal Inventory Form Processor
Copyright (c) 2017 Bart Massey

This is a random blob of code used to process Portland State
Unversity Computer Science Capstone "Personal Inventory"
forms as taken through Google Forms. I should probably
include the Form as well.

# License

This program is licensed under the "MIT License". Please see
the file `LICENSE` in this distribution for license terms.
