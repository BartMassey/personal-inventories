#!/usr/bin/python3
# Copyright (c) 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Format Capstone Personal Inventory for display.

import csv
import sys
import re

pi = open(sys.argv[1], "r")
responses = csv.reader(pi)
headings = next(responses)[:-2]

roles = []
for field in range(len(headings)):
    role_name = re.match("^Role: (.*)\. \(STRENGTH IN ROLE\)$", headings[field])
    if role_name:
        roles.append((field, role_name.group(1)))
role_types = ["Strength", "Preference", "Past Participation"]
skills_start = roles[-1][0] + 3
skills_end = skills_start
while headings[skills_end] != "Preference":
    skills_end += 1

system_preference = skills_end

additional_skills_start = system_preference + 1
additional_skills_headings = [
    "Course Info",
    "Group Work Difficulties",
    "Professional Group Work"]

reroles_start = additional_skills_start + len(additional_skills_headings)
reroles_minor = dict()
reroles_major = []
for field in range(reroles_start, len(headings)):
    major, minor = headings[field].split(": ")
    if major in reroles_minor:
        reroles_minor[major].append(minor)
    else:
        reroles_major.append(major)
        reroles_minor[major] = [minor]

index = []
for entry in responses:
    entry = entry[:-2]
    name = [s.title() for s in entry[1].split()]
    firstname, lastname = name
    name = firstname + " " + lastname
    undername = firstname + "_" + lastname
    filename = undername + ".mdwn"
    index.append((lastname, firstname))
    # http://www.diveintopython.net/scripts_and_streams/
    #   stdin_stdout_stderr.html
    with open("responses/" + filename, "w") as sys.stdout:
        date = entry[0].split()[0]
        print("# Personal Inventory:", name, "(" + date + ")")
        print()

        print("## Roles")
        print()
        for field, title in roles:
            print("*", title)
            for role_type in range(len(role_types)):
                print("  *", role_types[role_type] + ":",
                      entry[field + role_type])
        print()

        print("## Preferences")
        print()
        for field in range(skills_start, skills_end):
            print("*", headings[field] + ":", entry[field])
        print()
        syspref = re.sub(";", " or ", entry[system_preference])
        print("System Preference:", syspref)
        print()
        for field in range(len(additional_skills_headings)):
            print("###", additional_skills_headings[field])
            print()
            print(entry[additional_skills_start + field])
            print()

        print("## More Role Information")
        print()
        field = reroles_start
        for major in reroles_major:
            print("*", major)
            for minor in reroles_minor[major]:
                print("  *", minor + ":", entry[field])
                field += 1
            print()

with open("responses/index.mdwn", "w") as sys.stdout:
    print("# Personal Inventory Surveys")
    print()
    for lastname, firstname in sorted(index):
        print("* [[%s_%s]]" % (firstname, lastname))
