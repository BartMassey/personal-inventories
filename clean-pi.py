#!/usr/bin/python3
# Copyright (c) 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Clean Capstone Personal Inventory for delivery.

import csv
import sys

pi = open(sys.argv[1], "r")
responses = csv.reader(pi)
outf = csv.writer(sys.stdout)
for r in responses:
   outf.writerow(r[:-2])
