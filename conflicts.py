#!/usr/bin/python3
# Copyright (c) 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


import csv
import sys

reader = csv.reader(sys.stdin)
for entry in reader:
    conflicts = entry[-2]
    if conflicts:
        print(entry[1] + ":")
        print(conflicts)
